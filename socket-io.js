const io = require('socket.io-client');

const socket = io('wss://gps.marchd.ru/', {
    path: '/io'
});

socket.on('connect', function (data) {
    console.log('socket: connect', data);
});
socket.on('reconnect_attempt', function (data) {
    console.log('socket: reconnect_attempt', data);
});
