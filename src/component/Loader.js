import React, {Component} from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import * as colors from 'material-ui/styles/colors';

const loadedColor = [
    colors.red800,
    colors.pink800,
    colors.purple800,
    colors.deepPurple800,
    colors.indigo800,
    colors.blue800,
    colors.lightBlue800,
    colors.cyan800,
    colors.teal800,
    colors.green800,
    colors.lightGreen800,
    colors.lime800,
    colors.yellow800,
    colors.amber800,
    colors.deepOrange800,
    colors.red800,
];

export default class Loader extends Component {
    constructor(props) {
        super(props);
        this.color = loadedColor[parseInt(Math.random() * loadedColor.length)];
    }

    render() {
        return (
            <div className="loader">
                <div>
                    <CircularProgress color={this.color} size={160} thickness={5}/>
                </div>
            </div>
        );
    }
}