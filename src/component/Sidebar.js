import React, {Component} from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';

export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        }
    }

    render() {
        return (
            <Drawer
                docked={false}
                width={200}
                open={this.props.open}
                onRequestChange={this.props.onRequestChange ? this.props.onRequestChange : null}
            >
                {/*<AppBar title="Car Track" />*/}
                <MenuItem onClick={this.props.getDatTrack}>Получить трек за сутки</MenuItem>
                <DatePicker hintText="Portrait Dialog" style={{display: 'none'}}/>
            </Drawer>
        );
    }
}
