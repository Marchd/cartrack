import React, {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as trackActions from '../actions/trackActions';

import './App.css';
import Map from './Map';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import * as colors from 'material-ui/styles/colors';

import AppBar from 'material-ui/AppBar';
import Sidebar from '../component/Sidebar'
import IconButton from 'material-ui/IconButton';
import GPSNotFixed from 'material-ui/svg-icons/device/gps-not-fixed';
import GPSFixed from 'material-ui/svg-icons/device/gps-fixed';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#212121',
        primary2Color: '#212121',
        primary3Color: '#212121',
        primaryColor: '#212121',
        accent1Color: colors.red700,
        accent2Color: colors.red700,
        accent3Color: colors.red700,
        disabledColor: colors.grey700
    },
    textField: {
        borderBottom: '1px solid #E0E0E0',
    }
});

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sidebarOpen: false,
            follow: false
        };

        this.handleGPSFixedMod = this.handleGPSFixedMod.bind(this);
        this.onRequestChange = this.onRequestChange.bind(this);
        this.getDatTrack = this.getDatTrack.bind(this);
    }

    onRequestChange() {
        this.setState((privState) => ({sidebarOpen: !privState.sidebarOpen}));
    }

    getDatTrack() {
        this.props.trackActions.fetch();
        this.onRequestChange();
    }

    handleGPSFixedMod() {
        this.setState((privState) => ({follow: !privState.follow}));
    }

    render() {
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div className="flex-container-column">
                    <header>
                        <AppBar
                            title="Car Track"
                            onLeftIconButtonClick={this.onRequestChange}
                            iconElementRight={(
                                <IconButton onClick={this.handleGPSFixedMod}>
                                    {this.state.follow ? (
                                        <GPSFixed/>
                                    ) : (
                                        <GPSNotFixed/>
                                    )}
                                </IconButton>
                            )}
                            // iconClassNameRight="muidocs-icon-navigation-expand-more"
                        />
                        {/*<RaisedButton onClick={this.props.trackActions.fetch} fullWidth primary*/}
                        {/*label="Получить трек за сутки"/>*/}
                    </header>
                    <aside>
                        <Sidebar open={this.state.sidebarOpen}
                                 getDatTrack={this.getDatTrack}
                                 onRequestChange={this.onRequestChange}/>
                    </aside>
                    <div className="map-container">
                        <Map follow={this.state.follow}/>
                    </div>
                    <footer>
                    </footer>
                </div>
            </MuiThemeProvider>
        );
    }
}

function mapStateToProps(state) {
    return {
        // car: state.car
    }
}

function mapDispatchToProps(dispatch) {
    return {
        trackActions: bindActionCreators(trackActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

