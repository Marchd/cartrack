import React, {Component} from 'react';
import {YMaps, Map, ZoomControl, GeolocationControl} from 'react-yandex-maps';
import {connect} from 'react-redux'

import MapElements from './MapElements';

import Loader from '../component/Loader';

class MapContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            center: null
        };

        this.handleChangeOrientation = this.handleChangeOrientation.bind(this);

        window.addEventListener("orientationchange", this.handleChangeOrientation);
    }

    handleChangeOrientation(event) {
        alert('handleChangeOrientation')

    }

    componentDidMount() {
        // this.props.trackActions.fetch();
        // setInterval(this.props.carActions.fetch, 500);
    }

    componentWillUnmount() {
        // clearInterval(this.carUpdateInterval);
        window.removeEventListener("orientationchange", this.handleChangeOrientation);
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.follow && this.state.center === null && nextProps.car.coordinates !== null) {
            this.setState((prevState) => ({
                center: [...nextProps.car.coordinates]
            }))
        }
        else {
            this.setState((prevState) => ({
                center: [...nextProps.car.coordinates]
            }))
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.follow) return true;
        return this.state.center === null;
    }

    render() {
        let w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            mapX = w.innerWidth || e.clientWidth || g.clientWidth,
            mapY = w.innerHeight || e.clientHeight || g.clientHeight;
        return (
            <React.Fragment>
                {this.state.center ? (
                    <YMaps style={{
                        width: '100%',
                        height: '100%'
                    }}>
                        <Map width={mapX} height={mapY - 64} state={{
                            center: this.state.center,
                            zoom: 17,
                            controls: [],
                        }}>
                            <ZoomControl/>
                            <GeolocationControl/>
                            <MapElements/>
                        </Map>
                    </YMaps>
                ) : (
                    <div>
                        <Loader/>
                    </div>
                )}

            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        car: state.car,
        // track: state.track,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        // carActions: bindActionCreators(carActions, dispatch),
        // trackActions: bindActionCreators(trackActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapContainer)
