import React, {Component} from 'react';
import {YMaps, Map, Placemark, Polyline} from 'react-yandex-maps';
import {connect} from 'react-redux'

import * as carActions from '../actions/carActions';
import * as trackActions from '../actions/trackActions';

import tinygradient from 'tinygradient';

import {bindActionCreators} from 'redux'

class MapElements extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        let gradient = tinygradient([
            // '#FFFFFF',
            // '#000000',
            '#2196F3',
            '#03A9F4',
            '#00BCD4',
            '#009688',
            '#4CAF50',
            '#8BC34A',
            '#CDDC39',
            '#FFEB3B',
            '#FFC107',
            '#FF9800',
            '#FF5722',
            '#F44336',
        ]);
        gradient = gradient.rgb(24).map(item => {
            return `rgb(${item._r.toFixed(0)},${item._g.toFixed(0)},${item._b.toFixed(0)})`;
        });
        let hourWayParts = [];

        // let coordLine = '';
        // this.props.track.forEach((item, index, array) => {
        //     if (index < 50) {
        //         coordLine += `${item[0]},${item[1]}`;
        //         // if (index !== array.length - 1) coordLine += ';'
        //         if (index !== 49) coordLine += ';'
        //     }
        // });
        //
        // console.log('coordLine:');
        // console.log(coordLine);

        for (let i = 0; i < this.props.track.length; i++) {
            let partIDX = parseInt(i / (this.props.track.length / 24));
            if (!hourWayParts[partIDX]) hourWayParts[partIDX] = [];
            hourWayParts[partIDX] = [...hourWayParts[partIDX], this.props.track[i]];
            if (i + 1 < this.props.track.length && parseInt((i + 1) / (this.props.track.length / 24)) !== partIDX) {
                hourWayParts[partIDX] = [...hourWayParts[partIDX], this.props.track[i + 1]];
            }
        }
        return (
            <React.Fragment>
                <Placemark geometry={{coordinates: this.props.car.coordinates}}
                           properties={{
                               balloonContent: this.props.car.time
                           }}
                           options={{
                               preset: "islands#blueAutoIcon",
                               iconCaptionMaxWidth: "50"
                           }}
                />
                {hourWayParts.map((item, index) => {
                    return (
                        <Polyline key={index}
                                  geometry={{
                                      coordinates: item,
                                  }}
                                  properties={{
                                      // The contents of the balloon.
                                      // balloonContent: 'Ломаная линия',
                                  }}
                                  options={{
                                      balloonCloseButton: false,
                                      strokeColor: gradient[index],
                                      strokeWidth: 4,
                                      strokeOpacity: 0.75,
                                  }}
                        />
                    );
                })}
                {/*<Polyline*/}
                {/*geometry={{*/}
                {/*// Specifying the coordinates of the vertices of the polyline.*/}
                {/*coordinates: this.props.track,*/}
                {/*}}*/}
                {/*// Describing the properties of the geo object.*/}
                {/*properties={{*/}
                {/*// The contents of the balloon.*/}
                {/*// balloonContent: 'Ломаная линия',*/}
                {/*}}*/}
                {/*// Setting options for the geo object.*/}
                {/*options={{*/}
                {/*// Disabling the close button on a balloon.*/}
                {/*balloonCloseButton: false,*/}
                {/*// The line color.*/}
                {/*strokeColor: '#000000',*/}
                {/*// Line width.*/}
                {/*strokeWidth: 4,*/}
                {/*// The transparency coefficient.*/}
                {/*strokeOpacity: 0.5,*/}
                {/*}}*/}
                {/*/>*/}
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        car: state.car,
        track: state.track,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        carActions: bindActionCreators(carActions, dispatch),
        trackActions: bindActionCreators(trackActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapElements)
