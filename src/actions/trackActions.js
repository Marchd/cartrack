import network from '../modules/network';

export function fetch() {
    return async function (dispatch) {
        let date = new Date();
        date.setDate(date.getDate() - 1);
        return network.send('getTrack', null, {
            startdate: date.toISOString(),
            enddate: new Date().toISOString()
        }).then((data) => {
            dispatch({
                type: 'TRACK/FETCH',
                data: data
            });
            return data;
        }).catch((error) => {
            return error;

        });
    }
}
