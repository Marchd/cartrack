import network from '../modules/network';

export function fetch() {
    return async function (dispatch) {
        return network.send('getLastPoint', null, null, {
            method: 'get'
        }).then((data) => {
            dispatch({
                type: 'CAR/FETCH',
                data: data
            });
            return data;
        }).catch((error) => {
            return error;

        });
    }
}
