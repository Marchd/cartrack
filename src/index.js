import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './container/App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux'
import configureStore from './store/configureStore';
import io from 'socket.io-client';
// import NoSleep from 'nosleep.js';

const store = configureStore();

const socket = io('wss://gps.marchd.ru/gps', {
    path: '/io',
    query: {
        token: 'secret!',
    },

});

socket.on('connect', () => {
    console.log('socket connect');
});

socket.on('getLastPoint', (data) => {
    let objDate = JSON.parse(data.data);
    store.dispatch({
        type: 'CAR/FETCH',
        data: objDate
    });
    store.dispatch({
        type: 'TRACK/ADD',
        data: [objDate.coordinates]
    });
});

socket.on('disconnect', (data) => {
    console.log('socket disconnect', data);
});

socket.on('reconnect_attempt', (data) => {
    console.log('socket reconnect_attempt', data);
});

// const noSleep = new NoSleep();
// noSleep.enable();

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>, document.getElementById('root'));
registerServiceWorker();
