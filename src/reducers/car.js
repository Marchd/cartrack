const initialState = () => ({
    coordinates: null,
    time: Date.now()
});

export default function car(state = initialState(), action) {
    switch (action.type) {
        case 'CAR/FETCH': {
            return Object.assign({}, action.data);
        }

        case 'CAR/CLEAN': {
            return Object.assign({}, initialState());
        }

        default:
            return state;
    }
}
