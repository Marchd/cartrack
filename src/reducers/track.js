const initialState = () => ([]);

export default function car(state = initialState(), action) {
    switch (action.type) {
        case 'TRACK/FETCH': {
            return [...action.data];
        }

        case 'TRACK/ADD': {

            if (state.length) {
                let equality = action.data[0].filter((item, index) => {
                    return state[state.length - 1][index] !== item;
                });
                if (equality.length) {
                    return [...state, ...action.data]
                }
                else {
                    return state;
                }
            }
            else {
                return [...state, ...action.data];
            }
            // return [...state, ...action.data];
        }

        case 'TRACK/CLEAN': {
            return Object.assign({}, initialState());
        }

        default:
            return state;
    }
}
