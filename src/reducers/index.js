import {combineReducers} from 'redux';

import car from './car';
import track from './track';

export default combineReducers({
    car,
    track
});
