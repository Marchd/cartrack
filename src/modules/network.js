class Network {

    send(controller, action, data, options) {
        let defaultOptions = {
            json: true,
            method: 'post',
            mode: 'cors',
            headers: {
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Content-Type': 'application/json',
            },
            cache: 'no-cache',
            credentials: 'same-origin'
        };
        let init = Object.assign(defaultOptions, options);
        if (data) {
            if (init.json) {
                init = Object.assign(init, {
                    body: JSON.stringify(data)
                })
            }
            else {
                init = Object.assign(init, {
                    body: data
                })
            }
        }

        let url = `https://gps.marchd.ru/api` + ((controller) ? '/' + controller : '') + ((action) ? '/' + action : '');// + ((this._accessToken)?'?access-token='+this._accessToken:'');

        delete init.json;
        return fetch(url, init)
            .then(function (response) {

                try {
                    return response.json();
                }
                catch (e) {

                    return {};
                }
            })
            .then(function (result) {
                return result;
            });
    }
}

export default new Network();
